<?php
if(isset($_POST['firstname']) && isset($_POST['lastname']) && isset($_POST['email']) && isset($_POST['message'])) {

    require 'phpmailer/PHPMailerAutoload.php';

    $firstname = 	$_POST['firstname'];
    $lastname = 	$_POST['lastname'];
    $email = 		$_POST['email'];
    $message = 		$_POST['message'];
    $name = $firstname. ' '. $lastname;

    $mail = new PHPMailer;

    //$mail->SMTPDebug = 3;                               // Enable verbose debug output

    $mail->isSMTP();                                      // Set mailer to use SMTP
    $mail->Host = 'smtp.mailgun.org';  // Specify main and backup SMTP servers
    $mail->SMTPAuth = true;                               // Enable SMTP authentication
    $mail->Username = 'postmaster@cvallejo.me';                 // SMTP username
    $mail->Password = 'secret';                           // SMTP password
    $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
    $mail->Port = 587;                                    // TCP port to connect to

    $mail->From = $email;
    $mail->CharSet = "UTF-8";
    $mail->FromName = $name;
    $mail->addAddress("claudio.vallejo@gmail.com");

    $mail->isHTML(true);                                  // Set email format to HTML

    $mail->Subject = $name." sent you an email";
    $mail->Body = $message."<br><br>".$name."<br>".$email;
    //$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

    if (!$mail->send()) {
        echo 'Message could not be sent.';
        echo 'Mailer Error: ' . $mail->ErrorInfo;
    } else {
        echo 'Your message has been sent. Thanks ;)';
    }
}