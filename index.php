<!DOCTYPE html>
<html>
<head>
    <!-- ==========================
    	Meta Tags 
    =========================== -->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <!-- ==========================
    	Title 
    =========================== -->
    <title>Claudio Vallejo | cvallejo.me</title>
    
    <!-- ==========================
    	Favicons 
    =========================== -->
    <link rel="shortcut icon" href="icons/favicon.ico">
	<link rel="apple-touch-icon" href="icons/apple-touch-icon.png">
	<link rel="apple-touch-icon" sizes="72x72" href="icons/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="114x114" href="icons/apple-touch-icon-114x114.png">
    
    <!-- ==========================
    	Fonts 
    =========================== -->
    <link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Lilita+One' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Alegreya+Sans+SC' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>
  

    <!-- ==========================
    	CSS 
    =========================== -->
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="css/animate.css" rel="stylesheet" type="text/css">
    <link href="css/creative-brands.css" rel="stylesheet" type="text/css">
    <link href="css/vertical-carousel.css" rel="stylesheet" type="text/css">
    <link href="css/custom.css" rel="stylesheet" type="text/css">
    
    <!-- ==========================
    	JS 
    =========================== -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    
</head>
<body id="page-top">
	<a href="#page-top" class="scroll-up scroll"><i class="fa fa-chevron-up"></i></a>
    <h1>Freelancer</h1>
	
    <!-- ==========================
    	HEADER - START 
    =========================== -->
    <header>
        <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavigation"><i class="fa fa-bars"></i></button>
                    <img src="image/me.png" class="navbar-logo pull-left" alt="" />
                    <a href="/" class="navbar-brand animated flipInX">Claudio Vallejo</a>
                </div>
                
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="myNavigation">
                
                    <ul class="nav navbar-nav navbar-right animated">
                        <li><a href="#about" class="scroll">About me</a></li>
                        <li><a href="#showcase" class="scroll">My Work</a></li>
                        <li><a href="#contact" class="scroll">Contact</a></li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container -->
        </nav>
        
        <div class="jumbotron">
            <img src="image/me.png" class="img-responsive scrollpoint sp-effect3" alt="" />
            <div class="social-wrapper">
                <ul class="brands brands-inline hidden-xs scrollpoint sp-effect1">
                    <li><a href="https://www.facebook.com/claudio.vallejo.v"><i class="fa fa-facebook  hi-icon-effect-8"></i></a></li>
                    <li><a href="https://twitter.com/cvallejo" alt="@cvallejo"><i class="fa fa-twitter"></i></a></li>
                </ul>   
                <h2 class="scrollpoint sp-effect3">Claudio Vallejo</h2>
                <ul class="brands brands-inline hidden-xs scrollpoint sp-effect2">
                    <li><a href="https://github.com/cvallejo/"><i class="fa fa-github"></i></a></li>
                    <li><a href="https://www.linkedin.com/in/claudiovallejo"><i class="fa fa-linkedin"></i></a></li>
                </ul>       
            </div>
            <!-- Slideshow start -->                          
            <div id="slideshow" class="carousel slide vertical scrollpoint sp-effect3" data-ride="carousel">
                <div class="carousel-inner">
                    <!-- Slideshow item 1 active --> 
                    <div class="item active"><h3>I <i class="fa fa-heart icon-first"></i> wine</h3></div>
                    <!-- Slideshow item 2 -->     
                    <div class="item"><h3>Creative coder <i class="fa fa-code icon"></i></h3></div>
                    <!-- Slideshow item 3 -->   
                    <div class="item"><h3><i class="fa fa-trophy"></i> Entrepreneur</h3></div>
                </div>
            </div>
            <!-- Slideshow end -->   
        <a href="#about" class="btn btn-reference btn-lg scroll scrollpoint sp-effect1" role="button"><i class="fa fa-smile-o"></i> Know me better</a>
        <a href="#contact" class="btn btn-reference btn-lg btn-active scroll scrollpoint sp-effect2" role="button"><i class="fa fa-bolt"></i> Contact me</a>
        </div>
    </header>
    <!-- ==========================
    	HEADER - END 
    =========================== -->
     
    <!-- ==========================
    	ABOUT - START 
    =========================== -->
    <section id="about" class="content-first">
        <div class="container">
            <div class="row">
                <div class="col-sm-2 hidden-xs scrollpoint sp-effect1"><!-- Profile image --> 
                	<img src="image/me.png" class="image-bordered img-responsive" alt="" />
                </div><!-- Profile end --> 
                
                <div class="col-md-7 col-sm-6 scrollpoint sp-effect1"><!-- Profile description --> 
                	<h3>About me</h3>
                  	<p>
                        I'm involved in many different areas of the Chilean wine industry. His thesis was a complete analysis of the wine export market. He then collaborated in diverse wine projects for CORFO, the Chilean government's production development corporation. After this, he was in charge of generating periodic statistical analyses and information for CHILEVID, an association of producers of fine wines for exportation. He continued to expand his expertise in the wine industry as project engineer for the Corporación Chilena del Vino (Chilean Wine Corporation).
                    </p>
                    <p>
                        I'm also undertaken private ventures in the wine industry. His academic and professional experience in conjunction with his experience as an entrepreneur have given him a keen understanding of the value of opportune, accurate, and reliable information for Chilean wine producers. This insight led him to create the Wine Export Industry Intelligence Modular System in 2005. As a result of a complex process of induction, learning, and improvement, in 2007 this system evolved into today's INTELVID Ltda., a company pioneer in its field, designed to provide the Chilean wine export industry with statistical data.
                    </p>
                </div><!-- Profile description end-->
                
              <div class="col-md-3 col-sm-4 scrollpoint sp-effect2"><!--Timeline-->
                 
                  <div class="timeline">
                      <!-- BLOG POST 1 - START -->
                      <div class="event">
                          <div class="event-info">
                            <div class="date">2002</div>
                          </div>
                      	  <span>Graduated agronomist engineer</span>
                      </div> <!-- BLOG POST 1 - END -->
                      
                      <div class="event">
                          <div class="event-info">
                             <div class="date">2003</div>
                          </div>
                      	  <span>Chilean Wine Corporation - Project manager</span>
                      </div> <!-- BLOG POST 1 - END -->
                      
                      <div class="event">
                          <div class="event-info">
                             <div class="date">2004</div>
                          </div>
                      	  <span>Rodrigo Alvarado & Associates - CCO</span>
                      </div> <!-- BLOG POST 1 - END -->
                      
                      <div class="event">
                          <div class="event-info">
                             <div class="date">2006</div>
                          </div>
                      	  <span>INTELVID - Founder & CEO</span>
                      </div> <!-- BLOG POST 1 - END -->
                      
                      <div class="event">
                          <div class="event-info">
                             <div class="date">2013</div>
                          </div>
                      	  <span>Probulkwine - CoFounder</span>
                      </div> <!-- BLOG POST 1 - END -->

                  </div><!--timeline-->
              </div><!--col 3 end-->

            </div><!--Row about end-->


        </div><!-- Container end-->

    </section>
    <!-- ==========================
    	ABOUT - END 
    =========================== --> 

     <!-- ==========================
    	SHOWCASE - START
    =========================== -->   
    <section id="showcase" class="content-second">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h2 class="scrollpoint sp-effect3">My latest work</h2>
                    <p class="scrollpoint sp-effect3">Passionate, professional and trustworthy</p>
                </div>
                
                <div class="col-lg-6 showcase-text scrollpoint sp-effect1">
                    <div class="row">
                    <!-- Service 1 -->
                    	<div class="col-sm-6 col-md-6 col-lg-12">
                            <div class="media">
                                <div class="pull-left showcase-icon"><i class="fa fa-crosshairs"></i></div>
                                <div class="media-body">
                                    <h3 class="media-heading">Strategic Wine Source</h3>
                                    <p>Probulkwine, bring your wine business into the XXIst century! Bulk wine from Chile and Argentina... for now.</p>
                                </div>
                            </div>
                        </div>
                        <!-- Service 2 -->
                    	<div class="col-sm-6 col-md-6 col-lg-12">
                            <div class="media">
                                <div class="pull-left showcase-icon"><i class="fa fa-line-chart"></i></div>
                                <div class="media-body">
                                   <h3 class="media-heading">Wine Exports Industry</h3>
                                    <p>INTELVID aims to be a leader on an integral information provider service, where our basic principle is to build trustful alliances with our customers.</p>

                                </div>
                            </div>
                        </div>
                        <!-- Service 3 -->
                    	<div class="col-sm-6 col-md-6 col-lg-12">
                            <div class="media">
                                <div class="pull-left showcase-icon"><i class="fa fa-pie-chart"></i></div>
                                <div class="media-body">
                                    <h3 class="media-heading">Statistical Data</h3>
                                    <p>Development an intelligence system which allows its users access to associated statistical data of wine exports & imports.</p>
                                </div>
                            </div>
                        </div>
                        <!-- Service 4 -->
                    	<div class="col-sm-6 col-md-6 col-lg-12">
                            <div class="media">
                                <div class="pull-left showcase-icon"><i class="fa fa-code-fork"></i></div>
                                <div class="media-body">
                                    <h3 class="media-heading">Code & Development</h3>
                                    <p>Built web applications from the ground up using a modern programming language and MVC frameworks.</p>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>            
            
                <div class="col-lg-6 col-sm-8 col-md-8 col-md-offset-2 col-sm-offset-2 col-lg-offset-0 showcase-carousel hidden-xs scrollpoint sp-effect2">
                    <img src="image/imac.png" class="img-responsive" alt="">
                    
                    <div id="carousel-reference" class="carousel slide" data-ride="carousel">
                        <!-- Wrapper for slides -->
                        <div class="carousel-inner">
                            <div class="item active"><img src="image/image_01.png" alt=""></div>
                            <div class="item"><img src="image/image_02.png" alt=""></div>
                        </div>
                        
                        <!-- Controls -->
                        <a class="left carousel-control" href="#carousel-reference" data-slide="prev"><i class="fa fa-arrow-left"></i></a>
                        <a class="right carousel-control" href="#carousel-reference" data-slide="next"><i class="fa fa-arrow-right"></i></a>
                    </div>
                </div>  
            </div>
        </div>
    </section>
    <!-- ==========================
    	SHOWCASE - END 
    =========================== -->

    <!-- ==========================
    	NUMBERS - START
    =========================== -->
    <section id="separator" class="content-first">
        <div class="container">
            <div class="row scrollpoint sp-effect3">
                <div class="col-sm-2">
                    <i class="fa fa-user"></i>
                    <p><span class="number highlight" data-from="0" data-to="130" data-refresh-interval="10"></span> Happy clients</p>
                </div>
                <div class="col-sm-2">
                    <i class="fa fa-coffee"></i>
                    <p><span class="number highlight" data-from="0" data-to="850" data-refresh-interval="10"></span> cups of coffee</p>
                </div>
                <div class="col-sm-2">
                    <i class="fa fa-code"></i>
                    <p><span class="number highlight" data-from="0" data-to="75000" data-refresh-interval="100"></span> lines of codes</p>
                </div>
                <div class="col-sm-2">
                    <i class="fa fa-envelope"></i>
                    <p><span class="number highlight" data-from="0" data-to="26500" data-refresh-interval="50"></span> emails</p>
                </div>
                <div class="col-sm-2">
                    <i class="fa fa-phone"></i>
                    <p><span class="number highlight" data-from="0" data-to="175" data-refresh-interval="10"></span> hours phone calls</p>
                </div>
                <div class="col-sm-2">
                    <i class="fa fa-git-square"></i>
                    <p><span class="number highlight" data-from="0" data-to="380" data-refresh-interval="10"></span> days deploying</p>
                </div>
            </div>
        </div>
    </section>
    <!-- ==========================
    	NUMBERS - END
    =========================== -->


    
    <!-- ==========================
    	TESTIMONIALS - START
    =========================== -->   
    <section id="testimonials" class="content-first">
        <div class="container">
        	<h2 class="scrollpoint sp-effect3 text-center">Testimonials</h2>
        	<p class="scrollpoint sp-effect3 text-center">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce in turpis quam. Nulla quis eleifend lectus.</p> 
            <div class="row">
            	
                <!-- TESTIMONIAL 1 - START -->
                <div class="col-sm-6 scrollpoint sp-effect1">
                    <div class="media">
                        <a class="pull-left" href="#"><img class="media-object img-circle" src="image/tom.png" alt=""></a>
                        <div class="media-body">
                            <h4 class="media-heading">John Doy</h4>
                            <span>Webdesigner</span>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent sed volutpat est. Donec lobortis interdum volutpat. Maecenas bibendum dui quis pharetra tincidunt. Praesent posuere eu velit at scelerisque.</p>
                        </div>
                    </div>
                </div>
                <!-- TESTIMONIAL 1 - END -->
                
                <!-- TESTIMONIAL 2 - START -->
                <div class="col-sm-6 scrollpoint sp-effect2">
                    <div class="media">
                        <a class="pull-left" href="#"><img class="media-object img-circle" src="image/suzanne.png" alt=""></a>
                        <div class="media-body">
                            <h4 class="media-heading">Katie Mose</h4>
                            <span>Webdesigner</span>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent sed volutpat est. Donec lobortis interdum volutpat. Maecenas bibendum dui quis pharetra tincidunt. Praesent posuere eu velit at scelerisque.</p>
                        </div>
                    </div>
                </div>
                <!-- TESTIMONIAL 2 - END -->
                
            </div>
            
        </div>
    </section>
    <!-- ==========================
    	TESTIMONIALS - END 
    =========================== -->
    
    <!-- ==========================
    	CONTACT - START
    =========================== -->   
    <section id="contact" class="content-first">
        <div class="container">
        	<h2 class="scrollpoint sp-effect3">Contact me</h2>
            <div class="contact-alert">
            </div>
            <div class="row">
            <!-- PRICING TABLE FIRST - start --> 
                <div class="col-sm-12">
                	<img src="image/macbook.png" class="img-responsive hidden-xs" alt="">
                    
                    <div class="macbook-inner black">
                    	<div class="row scrollpoint sp-effect4">
                        	<div class="col-sm-8">
                            	<h3 class="hidden-xs">Leave a message</h3>
                                
                                <form role="form" id="contactForm">

                                    <div class="form-group control-group">
                                        <div class="controls">
                                            <p class="help-block"></p>
                                            <input type="text" class="form-control" placeholder="First Name" id="firstname" data-validation-required-message="Please enter your first name." aria-invalid="false">
                                        </div>
                                    </div>
                                    
                                    <div class="form-group control-group">
                                        <div class="controls">
                                            <p class="help-block"></p>
                                            <input type="text" class="form-control" placeholder="Last Name" id="lastname" data-validation-required-message="Please enter your last name." aria-invalid="false">
                                        </div>
                                    </div>
                                    
                                    <div class="form-group control-group">
                                        <div class="controls">
                                            <p class="help-block"></p>
                                            <input type="email" class="form-control" placeholder="Email" id="email" data-validation-required-message="Please enter your email address." aria-invalid="false">
                                        </div>
                                    </div>
                                    
                                    <div class="form-group control-group">
                                        <div class="controls">
                                            <p class="help-block"></p>
                                            <textarea class="form-control" placeholder="Message" id="message" data-validation-required-message="Please enter some message." aria-invalid="false"></textarea>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary">Send message</button>
                                    </div>
                                </form>
                                
                            </div>
                            <div class="col-sm-4">
                            
                            <img src="image/me.png" class="contact-image image-responsive hidden-xs hidden-sm" alt="" />
                            	<div class="contact-info">
                                	<p>Claudio Vallejo</p>
                                    <p><a href="https://github.com/cvallejo" style="color:#fff;"><i class="fa fa-github-square"></i> fork my repos</a></p>
                                    <p><a href="https://twitter.com/cvallejo" alt="@cvallejo" style="color:#fff;"><i class="fa fa-twitter-square"></i> follow me</a></p>
                                    <p><a href="https://www.linkedin.com/in/claudiovallejo" style="color:#fff;"><i class="fa fa-linkedin-square"></i> see my bio</a></p>
                                </div>
                            </div>
                    	</div>
                    </div>
                    
                </div>
            </div>
        </div>
    </section>
    <!-- ==========================
    	CONTACT - END 
    =========================== -->
    
    <!-- ==========================
    	FOOTER - START
    =========================== -->   
    <section id="footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 scrollpoint sp-effect3">
                
               			<ul class="brands brands-inline">
                            <li><a href="https://www.facebook.com/claudio.vallejo.v"><i class="fa fa-facebook  hi-icon-effect-8"></i></a></li>
                            <li><a href="https://twitter.com/cvallejo" alt="@cvallejo"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="https://github.com/cvallejo"><i class="fa fa-github-alt"></i></a></li>
                            <li><a href="https://www.linkedin.com/in/claudiovallejo"><i class="fa fa-linkedin"></i></a></li>
                        </ul>  
                    
                    <p>cvallejo.me</p>
                </div>   	
            </div>
        </div>
    </section>
    <!-- ==========================
    	FOOTER - END 
    =========================== -->
    
    <!-- ==========================
    	JS 
    =========================== -->
	<script src="http://code.jquery.com/jquery-latest.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/creative-brands.js"></script>
    <script src="js/jquery.easy-pie-chart.js"></script>
    <script src="js/modernizr.custom.js"></script>
    <script src="js/waypoints.min.js"></script>
    <script src="js/jquery.countTo.js"></script>
    <script src="js/jqBootstrapValidation.js"></script>
    <script src="js/custom.js"></script>
    <script>
    $(function () { 
		$("input,textarea").jqBootstrapValidation({
        	preventSubmit: true,
			
			submitSuccess: function($form, event) {
				event.preventDefault(); // prevent default submit behaviour
				// get values from FORM
				var firstname = $("input#firstname").val();
				var lastname = $("input#lastname").val();
				var email = $("input#email").val();
				var message = $("textarea#message").val();
	
				$.ajax({
					url: "send.php",
					type: "POST",
					data: {
						firstname: firstname,
						lastname: lastname,
						email: email,
						message: message
					},
					cache: false,
					success: function() {
						// Success message
						$('.contact-alert').html("<div class='alert alert-success'>");
						$('.contact-alert > .alert-success').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'><i class='fa fa-times'></i></button><strong>Your message has been sent.</strong></div>");
						//clear all fields
						$('#contactForm').trigger("reset");
					},
					error: function() {
						// Fail message
						$('.contact-alert').html("<div class='alert alert-danger'>");
						$('.contact-alert > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'><i class='fa fa-times'></i></button><strong>Sorry, it seems that my mail server is not responding. Please try again later!</strong></div>");
						//clear all fields
						$('#contactForm').trigger("reset");
					}
				})
			}
		});
	});
    </script>
    <!-- ==========================
    	GOOGLE ANALYTICS
    =========================== -->
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-62463433-1', 'auto');
        ga('send', 'pageview');

    </script>
    <!-- ==========================
    	END GOOGLE ANALYTICS
    =========================== -->
</body>
</html>